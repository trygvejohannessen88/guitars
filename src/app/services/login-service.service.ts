import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map, switchMap, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

const { apiUsers, apiKey } = environment

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  // dependency injection
  constructor(private readonly http: HttpClient) { }

  // models , HttpClient, obserables rxJS operators
  public login(username: string): Observable<User> {
    return this.checkUsername(username)
      .pipe(
        switchMap((user: User | undefined) => {
          if (user === undefined) {
            return this.createUser(username); 
          }
          return of(user);
        })
      ) 
  }

  private checkUsername(username: string): Observable<User | undefined> {
    return this.http.get<User[]>(`${apiUsers}?username=${username}`)
    .pipe(
      // RxJS Operators
      map((response: User[]) => response.pop())
    )
  }

  public createUser(username: string): Observable<User> {
    const user = {
      username,
      favourites: []
    };
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    })
    return this.http.post<User>(apiUsers, user, { headers })
  }

  // store user
}
